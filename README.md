## CMD

### clone this Repo

```bash
git clone https://nishitai@bitbucket.org/nishitai/docker-compose-mysql.git
```

### build and up

```bash
docker-compose up -d
```

### check

```bash
docker-compose ps

     Name                    Command             State                 Ports
-------------------------------------------------------------------------------------------
mysql-sample_db_1   docker-entrypoint.sh mysqld   Up      0.0.0.0:3314->3306/tcp, 33060/tcp
```


### attach to docker

```bash
docker exec -it sample-project_db_1 bash
```

### attach to mysql

```bash
mysql -u user -p
```

```mysql
mysql> use sample_db;
mysql> show tables;
+---------------------+
| Tables_in_sample_db |
+---------------------+
| users               |
+---------------------+

mysql> select * from users;
+----+------+-----------------+
| id | name | email           |
+----+------+-----------------+
|  1 | TOM  | xxxx@mail.co.jp |
+----+------+-----------------+

```

### attach mysql from host machine

```bash
mysql --port 3314 -u user -p
```